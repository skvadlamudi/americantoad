package svadlamudi.americantoad.tests;


import ks.client.gamefactory.GameWindow;
import ks.common.model.Deck;
import ks.launcher.Main;
import svadlamudi.AmericanToad;

/**
 *
 */
public abstract class AmericanToadTest extends ks.tests.KSTestCase {

    protected AmericanToad game;
    protected GameWindow gw;

    protected void setUp() {
        game = new AmericanToad();
        gw = Main.generateWindow(game, Deck.OrderBySuit);
    }

    protected void tearDown() {
        gw.setVisible(false);
        gw.dispose();
    }
}
