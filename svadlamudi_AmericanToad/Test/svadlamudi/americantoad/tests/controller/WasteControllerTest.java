package svadlamudi.americantoad.tests.controller;

import java.awt.event.MouseEvent;

import ks.common.model.Card;
import svadlamudi.americantoad.controller.WasteController;
import svadlamudi.americantoad.tests.AmericanToadTest;

public class WasteControllerTest extends AmericanToadTest {
	
	protected WasteController wc;
	
	public void testDblClick() {
		for(int i = 0; i <= 8; i++) {
			MouseEvent pr = createDoubleClicked(game, game.getWasteView(), 0, 0);
			
			game.getWaste().add(new Card(game.getFoundationRank(), 3));
			
			game.getWasteView().getMouseManager().handleMouseEvent(pr);
			
			assertTrue(game.getWaste().empty());
			assertTrue(game.getScoreValue() > 0);
			assertTrue(game.undoMove());
			assertTrue(game.getScoreValue() == 0);
			assertTrue(!game.getWaste().empty());
			
			super.setUp();
		}
	}
	
}
