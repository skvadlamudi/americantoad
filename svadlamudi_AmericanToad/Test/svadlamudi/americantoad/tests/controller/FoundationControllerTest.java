package svadlamudi.americantoad.tests.controller;

import java.awt.event.MouseEvent;

import ks.common.model.Card;
import ks.common.model.Column;
import ks.common.view.CardView;
import ks.common.view.ColumnView;
import svadlamudi.americantoad.tests.AmericanToadTest;

public class FoundationControllerTest extends AmericanToadTest {
	
	public void testReleaseCard() {
		for(int i = 1; i <= 8; i++) {
			MouseEvent pr = createReleased(game, game.getFoundationViewNum(i), 0, 0);
			
			Column col = new Column("tableau0");
			col.add(new Card(game.getFoundationRank(), 3));			
			ColumnView colView = new ColumnView(col);
			
			game.getContainer().setActiveDraggingObject(colView, pr);
			game.getContainer().setDragSource(colView);
			game.getFoundationViewNum(i).getMouseManager().handleMouseEvent(pr);
			assertTrue(game.getScoreValue() > 0);
			assertFalse(game.getFoundationNum(i).empty());
			assertTrue(game.undoMove());
			assertTrue(game.undoMove());
			assertTrue(game.getFoundationNum(i).empty());
			assertTrue(game.getScoreValue() == 0);
			super.setUp();
		}
	}
	
	public void testReleaseFoundation() {
		for(int i = 1; i <= 8; i++) {
			MouseEvent pr = createPressed(game, game.getFoundationViewNum(i), 0, 0);
			
			assertTrue(game.getContainer().getActiveDraggingObject() != null);
		}
	}
	
	public void testReleaseWaste() {
		for(int i = 1; i <= 8; i++) {
			MouseEvent pr = createReleased(game, game.getWasteView(), 0, 0);
			
			game.getContainer().setActiveDraggingObject(new CardView(new Card(game.getFoundationRank(), 3)), pr);
			game.getContainer().setDragSource(game.getWasteView());
			
			assertTrue(game.getFoundationNum(i).empty());
			game.getFoundationViewNum(i).getMouseManager().handleMouseEvent(pr);
			assertEquals(game.getFoundationNum(i).count(), 1);
			
		}
	}
	
	public void testReleaseReserve() {
		for(int i = 1; i <= 8; i++) {
			MouseEvent pr = createReleased(game, game.getReserveView(), 0, 0);
			
			game.getContainer().setActiveDraggingObject(new CardView(new Card(game.getFoundationRank(), 3)), pr);
			game.getContainer().setDragSource(game.getReserveView());
			
			assertTrue(game.getFoundationNum(i).empty());
			game.getFoundationViewNum(i).getMouseManager().handleMouseEvent(pr);
			assertEquals(game.getFoundationNum(i).count(), 1);
			
		}
	}
	
}
