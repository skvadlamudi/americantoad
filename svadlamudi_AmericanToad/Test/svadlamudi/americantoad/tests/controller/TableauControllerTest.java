package svadlamudi.americantoad.tests.controller;

import java.awt.event.MouseEvent;

import ks.common.model.Card;
import ks.common.model.Column;
import ks.common.view.ColumnView;
import svadlamudi.americantoad.tests.AmericanToadTest;
import svadlamudi.americantoad.util.Rank;

public class TableauControllerTest extends AmericanToadTest {
	
	public void testDblClick() {
		for(int i = 1; i <= 8; i++) {
			MouseEvent pr = createDoubleClicked(game, game.getTableauViewNum(i), 0, 0);
			game.getTableauNum(i).add(new Card(game.getFoundationRank(), 3));
			game.getTableauViewNum(i).getMouseManager().handleMouseEvent(pr);
			assertTrue(game.getScoreValue() > 0);
			assertFalse(game.getFoundationNum(1).empty());
			assertTrue(game.undoMove());
			assertTrue(game.getScoreValue() == 0);
			assertTrue(game.getFoundationNum(i).empty());
			super.setUp();
		}
	}
	
	public void testPressed() {
		for(int i = 1; i <= 8; i++) {
			MouseEvent pr = createPressed(game, game.getFoundationViewNum(i), 0, 0);
			game.getTableauViewNum(i).getMouseManager().handleMouseEvent(pr);
			assertFalse(game.getContainer().getActiveDraggingObject() == null);
		}
	}
	
	public void testReleased() {
		for(int i = 1; i <= 8; i++) {
			MouseEvent pr = createReleased(game, game.getTableauViewNum(i), 0, 0);
				
				Column col = new Column("tableau0");
				col.add(new Card(Rank.prevRank(game.getTableauNum(i).peek().getRank()), game.getTableauNum(i).peek().getSuit()));			
				ColumnView colView = new ColumnView(col);
				
				game.getContainer().setActiveDraggingObject(colView, pr);
				game.getContainer().setDragSource(colView);
				game.getTableauViewNum(i).getMouseManager().handleMouseEvent(pr);
				assertEquals(game.getTableauNum(i).count(), 2);
				assertTrue(game.undoMove());
				assertEquals(game.getTableauNum(i).count(), 1);
				super.setUp();
		}
	}

}
