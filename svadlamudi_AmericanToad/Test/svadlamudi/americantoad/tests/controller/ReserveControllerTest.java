package svadlamudi.americantoad.tests.controller;

import java.awt.event.MouseEvent;

import ks.common.model.Card;
import svadlamudi.americantoad.controller.ReserveController;
import svadlamudi.americantoad.tests.AmericanToadTest;

public class ReserveControllerTest extends AmericanToadTest {
	
	protected ReserveController rc;
	
	public void testDblClickToFoundation() {
		for(int i = 1; i <= 8; i++) {
			
			int score = game.getScoreValue();
			
			MouseEvent pr = createDoubleClicked(game, game.getReserveView(), 0, 0);
			
			game.getReserve().add(new Card(game.getFoundationRank(), 3));
			
			game.getReserveView().getMouseManager().handleMouseEvent(pr);
			
			assertEquals(game.getScoreValue(), score+1);
		}		
	}
	
}
