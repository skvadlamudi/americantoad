package svadlamudi.americantoad.tests.controller;

import java.awt.event.MouseEvent;

import svadlamudi.americantoad.controller.StockController;
import svadlamudi.americantoad.tests.AmericanToadTest;

public class StockControllerTest extends AmericanToadTest{
	
	protected StockController sc;
	
	public void testDealStock() {
		MouseEvent pr = createClicked(game, game.getStockView(), 0, 0);
		
		game.getStockView().getMouseManager().handleMouseEvent(pr);
		
		assertEquals(game.getWaste().count(), 1);
		assertTrue(game.undoMove());
		assertTrue(game.getWaste().empty());
	}
	
	public void testResetStock() {
		MouseEvent pr = createClicked(game, game.getStockView(), 0, 0);
		
		while(!game.getStock().empty())
			game.getWaste().add(game.getStock().get());
		
		game.getStockView().getMouseManager().handleMouseEvent(pr);
		
		assertEquals(game.getStock().count(), 76);
		assertTrue(game.undoMove());
		assertTrue(game.getStock().empty());
	}
	
}
