package svadlamudi.americantoad.tests.move;

import ks.common.model.Card;
import ks.common.model.Pile;
import svadlamudi.americantoad.move.ReserveFoundationMove;
import svadlamudi.americantoad.tests.AmericanToadTest;
import svadlamudi.americantoad.util.Rank;

/**
 * Test the Reserve to Foundation move.
 */
public class ReserveFoundationMoveTest extends AmericanToadTest {

    protected ReserveFoundationMove rfm;

    public void testDoMoveFoundationEmpty() {
        for (Pile foundation : game.getFoundations()) {

            Card goodCard = new Card(game.getFoundationRank(), 1);
            goodCard.setFaceUp(true);
            game.getReserve().add(goodCard);
            rfm = new ReserveFoundationMove(game.getReserve(), foundation, null);
            int score = game.getScoreValue();

            assertTrue(rfm.doMove(game));
            assertEquals(game.getScoreValue(), score + 1);

        }
    }

    public void testDoMoveFoundationHasOneCard() {
        for (int i = 0; i < 8; i++) {
            Card goodCard = new Card(game.getFoundationRank(), (i % 4) + 1);
            goodCard.setFaceUp(true);
            game.getFoundationNum(i + 1).add(goodCard);
            goodCard = new Card(Rank.nextRank(game.getFoundationRank()), (i % 4) + 1);
            game.getReserve().add(goodCard);
            rfm = new ReserveFoundationMove(game.getReserve(), game.getFoundationNum(i + 1), null);
            int score = game.getScoreValue();

            assertTrue(rfm.doMove(game));
            assertEquals(game.getScoreValue(), score + 1);
        }
    }

    public void testDoMoveFoundationEmptyDraggingCard() {
        for (int i = 0; i < 8; i++) {
            Card goodCard = new Card(game.getFoundationRank(), (i % 4) + 1);
            goodCard.setFaceUp(true);
            rfm = new ReserveFoundationMove(game.getReserve(), game.getFoundationNum(i+1), goodCard);
            int score = game.getScoreValue();

            assertTrue(rfm.doMove(game));
            assertEquals(game.getScoreValue(), score + 1);
        }
    }

    public void testDoMoveFoundationHasOneCardInvalid() {
        for (int i = 0; i < 8; i++) {
            Card goodCard = new Card(game.getFoundationRank(), (i % 4) + 1);
            goodCard.setFaceUp(true);
            game.getFoundationNum(i + 1).add(goodCard);
            Card badCard = new Card(game.getFoundationRank(), (i % 4) + 1);
            game.getReserve().add(badCard);
            rfm = new ReserveFoundationMove(game.getReserve(), game.getFoundationNum(i + 1), null);
            int score = game.getScoreValue();

            assertFalse(rfm.doMove(game));
            assertEquals(game.getScoreValue(), score);

            badCard = new Card(Rank.prevRank(game.getFoundationRank()), (i % 4) + 1);
            game.getReserve().add(badCard);
            rfm = new ReserveFoundationMove(game.getReserve(), game.getFoundationNum(i + 1), null);
            score = game.getScoreValue();

            assertFalse(rfm.doMove(game));
            assertEquals(game.getScoreValue(), score);
        }
    }

    public void testDoMoveWhereFoundationEmptyInvalid() {
        for (Pile foundation : game.getFoundations()) {

            Card badCard = new Card(Rank.nextRank(game.getFoundationRank()), 1);
            badCard.setFaceUp(true);
            game.getReserve().add(badCard);
            rfm = new ReserveFoundationMove(game.getReserve(), foundation, null);
            int score = game.getScoreValue();

            assertFalse(rfm.doMove(game));
            assertEquals(game.getScoreValue(), score);

            badCard = new Card(Rank.prevRank(game.getFoundationRank()), 1);
            game.getReserve().add(badCard);
            rfm = new ReserveFoundationMove(game.getReserve(), foundation, null);
            score = game.getScoreValue();

            assertFalse(rfm.doMove(game));
            assertEquals(game.getScoreValue(), score);

        }
    }

    public void testUndo() {
        for(int i = 0; i < 8; i++) {
            Card c = new Card(game.getFoundationRank(), (i%4)+1);
            c.setFaceUp(true);

            game.getFoundationNum(i+1).add(c);
            rfm = new ReserveFoundationMove(game.getReserve(), game.getFoundationNum(i+1), null);
            int score = game.getScoreValue();

            assertTrue(rfm.undo(game));
            assertEquals(game.getScoreValue(), score-1);
        }
    }

    public void testDoMoveFoundationEmptyDraggingCardInvalid() {
        for (int i = 0; i < 8; i++) {
            Card badCard = new Card(Rank.prevRank(game.getFoundationRank()), (i % 4) + 1);
            badCard.setFaceUp(true);
            rfm = new ReserveFoundationMove(game.getReserve(), game.getFoundationNum(i+1), badCard);
            int score = game.getScoreValue();

            assertFalse(rfm.doMove(game));
            assertEquals(game.getScoreValue(), score);

            badCard = new Card(Rank.nextRank(game.getFoundationRank()), (i % 4) + 1);
            rfm = new ReserveFoundationMove(game.getReserve(), game.getFoundationNum(i+1), badCard);
            score = game.getScoreValue();

            assertFalse(rfm.doMove(game));
            assertEquals(game.getScoreValue(), score);
        }
    }

    public void testDoMoveFoundationHasOneCardDraggingCardInvalid() {
        for (int i = 0; i < 8; i++) {
            Card goodCard = new Card(game.getFoundationRank(), (i % 4) + 1);
            goodCard.setFaceUp(true);
            game.getFoundationNum(i+1).add(goodCard);
            Card badCard = new Card(Rank.prevRank(game.getFoundationRank()), (i % 4) + 1);
            rfm = new ReserveFoundationMove(game.getReserve(), game.getFoundationNum(i+1), badCard);
            int score = game.getScoreValue();

            assertFalse(rfm.doMove(game));
            assertEquals(game.getScoreValue(), score);

            badCard = new Card(game.getFoundationRank(), (i % 4) + 1);
            rfm = new ReserveFoundationMove(game.getReserve(), game.getFoundationNum(i+1), badCard);
            score = game.getScoreValue();

            assertFalse(rfm.doMove(game));
            assertEquals(game.getScoreValue(), score);
        }
    }

}
