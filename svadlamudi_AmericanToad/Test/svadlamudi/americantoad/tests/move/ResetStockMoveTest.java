package svadlamudi.americantoad.tests.move;

import svadlamudi.americantoad.move.ResetStockMove;
import svadlamudi.americantoad.tests.AmericanToadTest;

public class ResetStockMoveTest extends AmericanToadTest {
	
	protected ResetStockMove rsm;
	
	public void setUp() {
		super.setUp();
		rsm = new ResetStockMove(game.getStock(), game.getWaste());
	}
	
	public void testMoveValid() {
		while(!game.getStock().empty())
			game.getWaste().add(game.getStock().get());
		assertTrue(rsm.doMove(game));
	}
	
	public void testUndoValid() {
		while(!game.getStock().empty())
			game.getWaste().add(game.getStock().get());
		
		rsm.doMove(game);
		
		assertTrue(rsm.undo(game));
	}
	
	public void testMoveStockNotEmpty() {
		for(int i = 1; i < game.getStock().count(); i++) {
			game.getWaste().add(game.getStock().get());
		}
		assertFalse(rsm.doMove(game));
	}
	
	public void testMoveStockNoRedealsLeft() {
		for(int i = 0; i < game.getStock().count(); i++) {
			game.getWaste().add(game.getStock().get());
		}
		
		rsm.doMove(game);
		
		for(int i = 0; i < game.getStock().count(); i++) {
			game.getWaste().add(game.getStock().get());
		}
		
		assertFalse(rsm.doMove(game));
	}
	
	public void testUndoInvalid() {
		assertFalse(rsm.undo(game));
	}
	
}
