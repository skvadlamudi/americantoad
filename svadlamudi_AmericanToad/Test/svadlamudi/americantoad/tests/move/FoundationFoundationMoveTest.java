package svadlamudi.americantoad.tests.move;

import ks.common.model.Card;
import svadlamudi.americantoad.move.FoundationFoundationMove;
import svadlamudi.americantoad.tests.AmericanToadTest;
import svadlamudi.americantoad.util.Rank;

public class FoundationFoundationMoveTest extends AmericanToadTest {
	
	protected FoundationFoundationMove ffm;
	
	public void testDoMoveNoCard() {
		for(int i = 1; i <= 8; i++) {
			for(int j = 1; j <= 8; j++) {
				if(i != j) {
					Card goodCard = new Card(game.getFoundationRank(), 3);
					goodCard.setFaceUp(true);
					game.getFoundationNum(i).add(goodCard);
					ffm = new FoundationFoundationMove(game.getFoundationNum(i), game.getFoundationNum(j), null);
					assertTrue(ffm.doMove(game));
					assertFalse(game.getFoundationNum(j).empty());
					assertTrue(ffm.undo(game));
					assertTrue(game.getFoundationNum(j).empty());
					super.setUp();		
				}
			}
		}
		super.setUp();
		for(int i = 1; i <= 8; i++) {
			for(int j = 1; j <= 8; j++) {
				if(i != j) {
					Card goodCard = new Card(game.getFoundationRank(), 3);
					goodCard.setFaceUp(true);
					ffm = new FoundationFoundationMove(game.getFoundationNum(i), game.getFoundationNum(j), goodCard);
					assertTrue(ffm.doMove(game));
					assertFalse(game.getFoundationNum(j).empty());
					assertTrue(ffm.undo(game));
					assertTrue(game.getFoundationNum(j).empty());
					super.setUp();		
				}
			}
		}
	}
	
	public void doMoveExistingCard() {
		for(int i = 1; i <= 8; i++) {
			for(int j = 1; j <= 8; j++) {
				if(i != j) {
					Card goodCard = new Card(game.getFoundationRank(), 3);
					goodCard.setFaceUp(true);
					game.getFoundationNum(j).add(new Card(Rank.prevRank(goodCard.getRank()), goodCard.getSuit()));
					game.getFoundationNum(i).add(goodCard);
					ffm = new FoundationFoundationMove(game.getFoundationNum(i), game.getFoundationNum(j), null);
					assertTrue(ffm.doMove(game));
					assertEquals(game.getFoundationNum(j).count(), 2);
					assertTrue(ffm.undo(game));
					assertEquals(game.getFoundationNum(j).count(), 1);
					super.setUp();		
				}
			}
		}
		super.setUp();
		for(int i = 1; i <= 8; i++) {
			for(int j = 1; j <= 8; j++) {
				if(i != j) {
					Card goodCard = new Card(game.getFoundationRank(), 3);
					goodCard.setFaceUp(true);
					game.getFoundationNum(j).add(new Card(Rank.prevRank(goodCard.getRank()), goodCard.getSuit()));
					ffm = new FoundationFoundationMove(game.getFoundationNum(i), game.getFoundationNum(j), goodCard);
					assertTrue(ffm.doMove(game));
					assertEquals(game.getFoundationNum(j).count(), 2);
					assertTrue(ffm.undo(game));
					assertEquals(game.getFoundationNum(j).count(), 1);
					super.setUp();		
				}
			}
		}
	}

}
