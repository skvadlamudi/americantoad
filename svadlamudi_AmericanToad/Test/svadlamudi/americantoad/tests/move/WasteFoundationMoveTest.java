package svadlamudi.americantoad.tests.move;

import ks.common.model.Card;
import svadlamudi.americantoad.move.WasteFoundationMove;
import svadlamudi.americantoad.tests.AmericanToadTest;
import svadlamudi.americantoad.util.Rank;

public class WasteFoundationMoveTest extends AmericanToadTest {
	
	protected WasteFoundationMove wfm;
	
	public void testWasteFoundationValid() {
		for(int i = 1; i <= 8; i++) {
			Card goodCard = new Card(game.getFoundationRank(), i%4+1);
			goodCard.setFaceUp(true);
			game.getWaste().add(goodCard);
			wfm = new WasteFoundationMove(game.getWaste(), game.getFoundationNum(i), null);
			assertTrue(wfm.doMove(game));
		}
		super.setUp();
		for(int i = 1; i <= 8; i++) {
			Card goodCard = new Card(game.getFoundationRank(), i%4+1);
			goodCard.setFaceUp(true);
			wfm = new WasteFoundationMove(game.getWaste(), game.getFoundationNum(i), goodCard);
			assertTrue(wfm.doMove(game));
		}
	}
	
	public void testUndoValid() {
		for(int i = 1; i <= 8; i++) {
			Card goodCard = new Card(game.getFoundationRank(), i%4+1);
			goodCard.setFaceUp(true);
			game.getFoundationNum(i).add(goodCard);
			wfm = new WasteFoundationMove(game.getWaste(), game.getFoundationNum(i), null);
			assertTrue(wfm.undo(game));
		}
	}
	
	public void testWasteFoundationInValid() {
		for(int i = 1; i <= 8; i++) {
			Card badCard = new Card(Rank.prevRank(game.getFoundationRank()), i%4+1);
			badCard.setFaceUp(true);
			game.getWaste().add(badCard);
			wfm = new WasteFoundationMove(game.getWaste(), game.getFoundationNum(i), null);
			assertFalse(wfm.doMove(game));
		}
		super.setUp();
		for(int i = 1; i <= 8; i++) {
			Card badCard = new Card(game.getFoundationRank(), i%4+1);
			badCard.setFaceUp(false);
			wfm = new WasteFoundationMove(game.getWaste(), game.getFoundationNum(i), badCard);
			assertFalse(wfm.doMove(game));
		}
	}
	
	public void testUndoInValid() {
		for(int i = 1; i <= 8; i++) {
			wfm = new WasteFoundationMove(game.getWaste(), game.getFoundationNum(i), null);
			assertFalse(wfm.undo(game));
		}
	}
}
