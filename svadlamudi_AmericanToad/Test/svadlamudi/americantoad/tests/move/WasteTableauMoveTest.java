package svadlamudi.americantoad.tests.move;

import ks.common.model.Card;
import svadlamudi.americantoad.move.WasteTableauMove;
import svadlamudi.americantoad.tests.AmericanToadTest;
import svadlamudi.americantoad.util.Rank;

public class WasteTableauMoveTest extends AmericanToadTest {
	
	protected WasteTableauMove wtm;
	
	public void testWasteTableauMoveValidNoDraggingCard() {
		for(int i = 1; i <= 8; i++) {
			Card goodCard = new Card(Rank.prevRank(game.getTableauNum(i).peek().getRank()), game.getTableauNum(i).peek().getSuit());
			goodCard.setFaceUp(true);
			game.getWaste().add(goodCard);
			wtm = new WasteTableauMove(game.getWaste(), game.getTableauNum(i), null);
			assertTrue(wtm.doMove(game));
		}
	}
	
	public void testWasteTableauMoveValid() {
		for(int i = 1; i <= 8; i++) {
			Card goodCard = new Card(Rank.prevRank(game.getTableauNum(i).peek().getRank()), game.getTableauNum(i).peek().getSuit());
			goodCard.setFaceUp(true);
			wtm = new WasteTableauMove(game.getWaste(), game.getTableauNum(i), goodCard);
			assertTrue(wtm.doMove(game));
		}
	}
	
	public void testUndoValid() {
		for(int i = 1; i <= 8; i++) {
			Card goodCard = new Card(Rank.prevRank(game.getTableauNum(i).peek().getRank()), game.getTableauNum(i).peek().getSuit());
			goodCard.setFaceUp(true);
			wtm = new WasteTableauMove(game.getWaste(), game.getTableauNum(i), goodCard);
			wtm.doMove(game);
			assertTrue(wtm.undo(game));
		}
	}
	
	public void testWasteTableauMoveInValidNoDraggingCard() {
		for(int i = 1; i <= 8; i++) {
			Card badCard = new Card(Rank.prevRank(game.getTableauNum(i).peek().getRank()), game.getTableauNum(i).peek().getSuit());
			badCard.setFaceUp(false);
			game.getWaste().add(badCard);
			wtm = new WasteTableauMove(game.getWaste(), game.getTableauNum(i), null);
			assertFalse(wtm.doMove(game));
		}
		
		super.setUp();
		
		for(int i = 1; i <= 8; i++) {
			Card badCard = new Card(Rank.nextRank(game.getTableauNum(i).peek().getRank()), game.getTableauNum(i).peek().getSuit());
			badCard.setFaceUp(true);
			game.getWaste().add(badCard);
			wtm = new WasteTableauMove(game.getWaste(), game.getTableauNum(i), null);
			assertFalse(wtm.doMove(game));
		}
	}
	
	public void testWasteTableauMoveInValid() {
		for(int i = 1; i <= 8; i++) {
			Card badCard = new Card(Rank.prevRank(game.getTableauNum(i).peek().getRank()), game.getTableauNum(i).peek().getSuit());
			badCard.setFaceUp(false);
			wtm = new WasteTableauMove(game.getWaste(), game.getTableauNum(i), badCard);
			assertFalse(wtm.doMove(game));
		}
		
		super.setUp();
		
		for(int i = 1; i <= 8; i++) {
			Card badCard = new Card(Rank.nextRank(game.getTableauNum(i).peek().getRank()), game.getTableauNum(i).peek().getSuit());
			badCard.setFaceUp(true);
			wtm = new WasteTableauMove(game.getWaste(), game.getTableauNum(i), badCard);
			assertFalse(wtm.doMove(game));
		}
	}
	
	public void testUndoInValid() {
		for(int i = 1; i <= 8; i++) {
			game.getTableauNum(i).get();
			wtm = new WasteTableauMove(game.getWaste(), game.getTableauNum(i), null);
			assertFalse(wtm.undo(game));
		}
	}
	
}
