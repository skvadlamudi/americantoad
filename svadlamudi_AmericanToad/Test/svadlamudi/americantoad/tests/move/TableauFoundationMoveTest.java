package svadlamudi.americantoad.tests.move;



import ks.common.model.Card;
import ks.common.model.Column;
import ks.common.model.Stack;
import svadlamudi.americantoad.move.TableauFoundationMove;
import svadlamudi.americantoad.tests.AmericanToadTest;
import svadlamudi.americantoad.util.Rank;

public class TableauFoundationMoveTest extends AmericanToadTest {

	protected TableauFoundationMove tfm;
	
	public void testTableauFoundationMoveNoDraggingCardValid() {
		for(int i = 1; i <= 8; i++) {			
			for(int j = 1; j <= 8; j++) {
				Card goodCard = new Card(game.getFoundationRank(), i%4+1);
				goodCard.setFaceUp(true);
				game.getTableauNum(i).add(goodCard);
				tfm = new TableauFoundationMove(game.getTableauNum(i), game.getFoundationNum(j), null);
				assertTrue(tfm.doMove(game));
			}
			super.setUp();
		}
	}
	
	public void testTableauFoundationMoveValid() {
		for(int i = 1; i <= 8; i++) {			
			for(int j = 1; j <= 8; j++) {
				Card goodCard = new Card(game.getFoundationRank(), i%4+1);
				goodCard.setFaceUp(true);
				Stack tempStack = new Stack();
				tempStack.add(goodCard);
				tfm = new TableauFoundationMove(game.getTableauNum(i), game.getFoundationNum(j), new Column(tempStack));
				assertTrue(tfm.doMove(game));
			}
			super.setUp();
		}
	}
	
	public void testTableauFoundationMoveNoDraggingCardInValid() {
		for(int i = 1; i <= 8; i++) {			
			for(int j = 1; j <= 8; j++) {
				Card goodCard = new Card(Rank.prevRank(game.getFoundationRank()), i%4+1);
				goodCard.setFaceUp(true);
				game.getTableauNum(i).add(goodCard);
				tfm = new TableauFoundationMove(game.getTableauNum(i), game.getFoundationNum(j), null);
				assertFalse(tfm.doMove(game));
			}
		}
	}
	
	public void testUndoInvalid() {
		for(int i = 1; i <= 8; i++) {			
			for(int j = 1; j <= 8; j++) {
				tfm = new TableauFoundationMove(game.getTableauNum(i), game.getFoundationNum(j), null);
				assertFalse(tfm.undo(game));
			}
		}
	}
	
}
