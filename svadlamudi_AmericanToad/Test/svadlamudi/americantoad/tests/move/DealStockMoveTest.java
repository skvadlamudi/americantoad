package svadlamudi.americantoad.tests.move;

import svadlamudi.americantoad.move.DealStockMove;
import svadlamudi.americantoad.tests.AmericanToadTest;

/**
 * Test the Stock to Waste deal move.
 */
public class DealStockMoveTest extends AmericanToadTest {

    protected DealStockMove dsm;

    @Override
    public void setUp() {
        super.setUp();
        dsm = new DealStockMove(game.getStock(), game.getWaste());
    }

    /**
     * Test that given:
     *  - Stock is not empty.
     *
     * The move is seen as valid.
     */
    public void testValid() {
        assertTrue(dsm.valid(game));
        assertTrue(!game.getStock().empty());
    }

    /**
     * Test that given:
     *  - Stock is empty.
     *
     *  The move is seen as invalid.
     */
    public void testValidInvalid() {
        game.getStock().removeAll();
        assertFalse(dsm.valid(game));
    }

    /**
     * Test that given:
     *  - Stock is not empty.
     *
     *  The move is performed.
     */
    public void testDoMove() {
        assertEquals(game.getStock().count(), 76);
        assertEquals(game.getWaste().count(), 0);

        assertTrue(dsm.doMove(game));

        assertEquals(game.getStock().count(), 75);
        assertEquals(game.getWaste().count(), 1);

        assertTrue(dsm.undo(game));

        assertEquals(game.getStock().count(), 76);
        assertEquals(game.getWaste().count(), 0);
    }

    /**
     * Test that given:
     *  - Stock is empty.
     *
     *  The move is not performed.
     */
    public void testInvalidDoMove() {
        game.getStock().removeAll();
        assertFalse(dsm.doMove(game));
    }

    /**
     * Test that given:
     *  - Waste is not empty.
     *
     *  The move is performed.
     */
    public void testUndo() {
        assertEquals(dsm.undo(game), !game.getWaste().empty());
    }

    /**
     * Test that given:
     *  - Waste is empty.
     *
     *  The move is not performed.
     */
    public void testUndoInvalid() {
        game.getWaste().removeAll();
        assertFalse(dsm.undo(game));
    }

    @Override
    public void tearDown() {
        super.tearDown();
        dsm = null;
    }

}
