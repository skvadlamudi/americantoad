package svadlamudi.americantoad.tests.move;

import ks.common.model.Card;
import svadlamudi.americantoad.move.ReserveTableauMove;
import svadlamudi.americantoad.tests.AmericanToadTest;
import svadlamudi.americantoad.util.Rank;

public class ReserveTableauMoveTest extends AmericanToadTest {
	
	protected ReserveTableauMove rtm;
	
	public void testReserveTableauMoveValid() {
		for(int i = 1; i <= 8; i++) {
			rtm = new ReserveTableauMove(game.getReserve(), game.getTableauNum(i), null);
			
			Card goodCard = new Card(Rank.prevRank(game.getTableauNum(i).peek().getRank()), game.getTableauNum(i).peek().getSuit());
			game.getReserve().add(goodCard);
			
			assertTrue(rtm.doMove(game));
		}
	}
	
	public void testUndoValid() {
		for(int i = 1; i <= 8; i++) {
			rtm = new ReserveTableauMove(game.getReserve(), game.getTableauNum(i), null);
			
			Card goodCard = new Card(Rank.prevRank(game.getTableauNum(i).peek().getRank()), game.getTableauNum(i).peek().getSuit());
			game.getReserve().add(goodCard);
			
			assertTrue(rtm.doMove(game));
			assertTrue(rtm.undo(game));
		}
	}
	
	public void testReserveDraggingCardValid() {
		for(int i = 1; i <= 8; i++) {
			Card goodCard = new Card(Rank.prevRank(game.getTableauNum(i).peek().getRank()), game.getTableauNum(i).peek().getSuit());
			goodCard.setFaceUp(true);
			
			rtm = new ReserveTableauMove(game.getReserve(), game.getTableauNum(i), goodCard);
			
			assertTrue(rtm.doMove(game));
		}
	}
	
	public void testReserveTableauMoveInvalid() {
		for(int i = 1; i <= 8; i++) {
			rtm = new ReserveTableauMove(game.getReserve(), game.getTableauNum(i), null);
			
			Card badCard = new Card(Rank.nextRank(game.getTableauNum(i).peek().getRank()), game.getTableauNum(i).peek().getSuit());
			game.getReserve().add(badCard);
			
			assertFalse(rtm.doMove(game));
		}
	}
	
	public void testReserveFaceDownCardInvalid() {
		for(int i = 1; i <= 8; i++) {
			rtm = new ReserveTableauMove(game.getReserve(), game.getTableauNum(i), null);
			
			Card badCard = new Card(Rank.prevRank(game.getTableauNum(i).peek().getRank()), game.getTableauNum(i).peek().getSuit());
			badCard.setFaceUp(false);
			game.getReserve().add(badCard);
			
			assertFalse(rtm.doMove(game));
		}
	}
	
	public void testReserveDraggingCardInvalid() {
		for(int i = 1; i <= 8; i++) {
			Card badCard = new Card(Rank.prevRank(game.getTableauNum(i).peek().getRank()), game.getTableauNum(i).peek().getSuit());
			badCard.setFaceUp(false);
			
			rtm = new ReserveTableauMove(game.getReserve(), game.getTableauNum(i), badCard);
			
			assertFalse(rtm.doMove(game));
		}
	}
	
	public void testUndoInvalid() {
		for(int i = 1; i <= 8; i++) {
			rtm = new ReserveTableauMove(game.getReserve(), game.getTableauNum(i), null);
			
			game.getTableauNum(i).removeAll();
			
			assertFalse(rtm.undo(game));
		}
	}
	
}
