package svadlamudi.americantoad.tests.move;

import ks.common.model.Card;
import svadlamudi.americantoad.move.TableauTableauMove;
import svadlamudi.americantoad.tests.AmericanToadTest;
import svadlamudi.americantoad.util.Rank;

public class TableauTableauMoveTest extends AmericanToadTest {
	
	protected TableauTableauMove ttm;
	
	public void testSingleCardMove() {
		for(int i = 1; i <= 8; i++) {
			for(int j = 1; j <= 8; j++) {
				if(i == j)
					continue;
				Card goodCard = new Card(Rank.prevRank(game.getTableauNum(j).rank()), game.getTableauNum(j).peek().getSuit());
				goodCard.setFaceUp(true);
				game.getTableauNum(i).add(goodCard);
				ttm = new TableauTableauMove(game.getTableauNum(i), game.getTableauNum(j), null, 1);
				assertTrue(ttm.doMove(game));
				assertTrue(ttm.undo(game));
			}
		}
	}
	
	public void testMultiCardMove() {
		for(int i = 1; i <= 8; i++) {
			for(int j = 1; j <= 8; j++) {
				if(i == j)
					continue;
				Card goodCard = new Card(Rank.prevRank(game.getTableauNum(j).rank()), game.getTableauNum(j).peek().getSuit());
				goodCard.setFaceUp(true);
				game.getTableauNum(i).add(goodCard);
				goodCard = new Card(Rank.prevRank(goodCard.getRank()), goodCard.getSuit());
				game.getTableauNum(i).add(goodCard);
				ttm = new TableauTableauMove(game.getTableauNum(i), game.getTableauNum(j), null, 2);
				assertTrue(ttm.doMove(game));
				assertTrue(ttm.undo(game));
			}
		}
	}

}
