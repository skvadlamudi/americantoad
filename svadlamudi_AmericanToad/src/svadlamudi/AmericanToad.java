package svadlamudi;

import ks.client.gamefactory.GameWindow;
import ks.common.controller.SolitaireMouseMotionAdapter;
import ks.common.games.Solitaire;
import ks.common.games.SolitaireUndoAdapter;
import ks.common.model.*;
import ks.common.view.*;
import ks.launcher.Main;
import svadlamudi.americantoad.controller.*;
import svadlamudi.americantoad.util.Rank;

import java.awt.*;
import java.util.Random;

/**
 * Standard AmericanToad Plugin
 * <p>
 *     Layout: Deal two decks of cards to the stock.
 *     Deal 20 cards to the reserve with only the top card being faceup.
 *     Deal one card to each of the eight tableaus till none are empty and leave
 *     the remaining 76 cards in the stock pile.
 * </p>
 * <p>
 *     Object: To find the eight cards maching the foundation rank, and to
 *     build each up in suit and rank till all 13 cards of the suit are in
 *     the foundation.
 * </p>
 * <p>
 *     Play:
 * </p>
 */
public class AmericanToad extends Solitaire{

    public final int numPassesAllowed = 2;
	
	protected MultiDeck stock;
    protected Pile waste;
    protected Pile reserve;
    protected Column[] tableaus = new Column[9];
    protected Pile[] foundations = new Pile[9];
    protected DeckView stockView;
    protected PileView wasteView;
    protected PileView reserveView;
    protected ColumnView[] tableauViews = new ColumnView[9];
    protected PileView[] foundationViews = new PileView[9];
    protected MutableInteger score;
    protected MutableInteger numCardsLeft;
    protected IntegerView scoreView;
    private StringView scoreTextView;
    protected IntegerView numCardsLeftView;
    private StringView numCardsLeftTextView;
    private int numPasses;
    //private StringView numPassesView;
    private int foundationRank;
    private StringView foundationRankView;
    private IntegerView foundationRankNumView;

    // Accessor methods

    public MultiDeck getStock() { return stock; }
    
    public DeckView getStockView() { return stockView; }

    public Pile getWaste() { return waste; }
    
    public PileView getWasteView() { return wasteView; }

    public Pile getReserve() { return reserve; }
    
    public PileView getReserveView() { return reserveView; }

    public Pile[] getFoundations() {

        Pile[] tempFoundations = new Pile[8];
        for (int i = 1; i <= 8; i++) {
            tempFoundations[i-1] = foundations[i];
        }
        return tempFoundations;
    }

    public Pile getFoundationNum(int num) {
        if (num >= 1 && num <= 8)
            return foundations[num];

        return null;
    }
    
    public PileView getFoundationViewNum(int num) {
    	if (num >= 1 && num <= 8)
            return foundationViews[num];

        return null;
    }

    public Column[] getTableaus() { return tableaus; }
    
    public Column getTableauNum(int num) {
    	if (num >= 1 && num <= 8) {
    		return tableaus[num];
    	}
    	return null;
    }
    
    public ColumnView getTableauViewNum(int num) {
    	if (num >= 1 && num <= 8) {
    		return tableauViews[num];
    	}
    	return null;
    }

    public int getFoundationRank() {
        return foundationRank;
    }

    public int getNumPasses() {
        return numPasses;
    }

    @Override
    public String getName() { return "American Toad"; }

    @Override
    public boolean hasWon() { return score.getValue() == 104;}

    @Override
    public MutableInteger getNumLeft() {
        return numCardsLeft;
    }

    @Override
    public void initialize() {
        Random randomGenerator = new Random();
        foundationRank = randomGenerator.nextInt(13) + 1;

        initializeModel();
        initializeView();
        initializeControllers();

        container.refreshWidgets();


        // Deal 20 Cards to wasteView
        for (int num = 0; num < 19; num++) {
            Card c = stock.get();

            c.setFaceUp(false);
            reserve.add(c);
            numCardsLeft.increment(-1);
        }
        Card c = stock.get();
        c.setFaceUp(true);
        reserve.add(c);
        numCardsLeft.increment(-1);

        // Deal a Card to all tableaus
        for (int i = 1; i <= 8; i++) {
            c = stock.get();
            c.setFaceUp(true);
            tableaus[i].add(c);
            numCardsLeft.increment(-1);
        }
    }

    private void initializeModel() {
        stock = new MultiDeck("stock", 2);
        stock.create(getSeed());
        model.addElement(stock);

        waste = new Pile("waste");
        model.addElement(waste);

        reserve = new Pile("reserve");
        model.addElement(reserve);

        // Develop Foundations
        for (int i = 1; i <= 8; i++) {
            foundations[i] = new Pile ("foundation" + i);
            model.addElement(foundations[i]);
        }

        // Develop Tableaus
        for (int i = 1; i <= 8; i++) {
            tableaus[i] = new Column("tableau" + i);
            model.addElement(tableaus[i]);
        }

        score = new MutableInteger("score", 0);
        numCardsLeft = new MutableInteger("numCardsLeft", 104);
        numPasses = 0;

    }

    private void initializeControllers() {

        // Controller for the stock
        stockView.setMouseAdapter(new StockController(this, stock, waste));
        stockView.setMouseMotionAdapter(new SolitaireMouseMotionAdapter(this));
        stockView.setUndoAdapter(new SolitaireUndoAdapter(this));

        // Controller for the waste
        wasteView.setMouseAdapter(new WasteController(this, wasteView));
        wasteView.setMouseMotionAdapter(new SolitaireMouseMotionAdapter(this));
        wasteView.setUndoAdapter(new SolitaireUndoAdapter(this));

        // Controller for the waste
        reserveView.setMouseAdapter(new ReserveController(this, reserveView));
        reserveView.setMouseMotionAdapter(new SolitaireMouseMotionAdapter(this));
        reserveView.setUndoAdapter(new SolitaireUndoAdapter(this));

        // Controller for the foundations
        for(int i = 1; i <= 8; i++){
            foundationViews[i].setMouseAdapter(new FoundationController(this, foundationViews[i]));
            foundationViews[i].setMouseMotionAdapter(new SolitaireMouseMotionAdapter(this));
            foundationViews[i].setUndoAdapter(new SolitaireUndoAdapter(this));
        }

        // Controller for the tableaus
        for(int i = 1; i <= 8; i++){
            tableauViews[i].setMouseAdapter(new TableauController(this, tableauViews[i]));
            tableauViews[i].setMouseMotionAdapter(new SolitaireMouseMotionAdapter(this));
            tableauViews[i].setUndoAdapter(new SolitaireUndoAdapter(this));
        }
    }

    private void initializeView() {
        CardImages ci = getCardImages();

        // Develop stockView
        stockView = new DeckView(stock);
        stockView.setBounds(140 + 6 * ci.getWidth(), 20, ci.getWidth(), ci.getHeight());
        container.addWidget(stockView);

        // Develop wasteView
        wasteView = new PileView(waste);
        wasteView.setBounds(160 + 7 * ci.getWidth(), 20, ci.getWidth(), ci.getHeight());
        container.addWidget(wasteView);

        // Develop wasteView
        reserveView = new PileView(reserve);
        reserveView.setBounds(20, 20, ci.getWidth(), ci.getHeight());
        container.addWidget(reserveView);

        // Develop foundationViews
        for (int i = 1; i <= 8; i++){
            foundationViews[i] = new PileView(foundations[i]);
            foundationViews[i].setBounds((20 * i) + ((i - 1) * ci.getWidth()), 40 + ci.getHeight(), ci.getWidth(), ci.getHeight());
            container.addWidget(foundationViews[i]);
        }

        // Develop tableauViews
        for (int i = 1; i <= 8; i++){
            tableauViews[i] = new ColumnView(tableaus[i]);
            tableauViews[i].setBounds((20 * i) + ((i - 1) * ci.getWidth()), 60 + 2*ci.getHeight(), ci.getWidth(), 13*ci.getHeight());

            container.addWidget(tableauViews[i]);
        }

        // Develop scoreView
        scoreTextView = new StringView("Score:");
        scoreTextView.setFontSize(14);
        scoreTextView.setBounds(180 + 8 * ci.getWidth(), 20, 80, ci.getHeight()/3);
        scoreView = new IntegerView(score);
        scoreView.setBounds(180 + 8 * ci.getWidth(), 20+ci.getHeight() / 3, 110, (ci.getHeight() * 2) / 3);
        scoreView.setColor(Color.white);
        container.addWidget(scoreTextView);
        container.addWidget(scoreView);

        // Develop numCardsLeftView
        numCardsLeftTextView = new StringView("Cards Left:");
        numCardsLeftTextView.setFontSize(14);
        numCardsLeftTextView.setBounds(180 + 8 * ci.getWidth(), 40+ci.getHeight(), 110, ci.getHeight()/3);
        numCardsLeftView = new IntegerView(numCardsLeft);
        numCardsLeftView.setBounds(180 + 8 * ci.getWidth(), 40+ci.getHeight() + ci.getHeight() / 3, 110, (ci.getHeight() * 2) / 3);
        numCardsLeftView.setColor(Color.white);
        container.addWidget(numCardsLeftTextView);
        container.addWidget(numCardsLeftView);

        // Display Initial FoundationRank
        foundationRankView = new StringView("Foundations Rank: " + Rank.rankToText(foundationRank));
        foundationRankView.setFontSize(14);
        foundationRankView.setBounds(180 + 8 * ci.getWidth(), 60+2*ci.getHeight(), 190, ci.getHeight() / 3);
        container.addWidget(foundationRankView);
        foundationRankNumView = new IntegerView(new MutableInteger(foundationRank));
        foundationRankNumView.setBounds(180 + 8 * ci.getWidth(), 60+2 * ci.getHeight() + ci.getHeight() / 3, 110, ((ci.getHeight()) * 2) / 3);
        foundationRankNumView.setColor(Color.white);
        container.addWidget(foundationRankNumView);
        
        /*// Display Stock Passes Left
        numPassesView = new StringView("Stock Passes Left: " + (numPassesAllowed - numPasses));
        numPassesView.setFontSize(14);
        numPassesView.setBounds(180 + 8 * ci.getWidth(), 80+3*ci.getHeight(), 190, ci.getHeight() / 3);
        container.addWidget(numPassesView);*/
    }

    public void updateNumberCardsLeft(int delta) {
        numCardsLeft.setValue(numCardsLeft.getValue() + delta);
    }

    public void updateScore(int delta) {
        score.setValue(score.getValue() + delta);
    }
    
    public void updateNumPasses(int delta) {
    	numPasses += delta;
    }

    @Override
    public int getScoreValue() {
        return score.getValue();
    }

    public int getNumCardsLeftValue() {
        return numCardsLeft.getValue();
    }

    public Dimension getPreferredSize() {
        return new Dimension(970, 700);
    }

    /** Code to launch solitaire variation. */
    public static void main (String []args) {
        // Seed is to ensure we get the same initial cards every time.
        // Here the seed is to "order by suit."
        GameWindow gw  = Main.generateWindow(new AmericanToad(), Deck.OrderBySuit);
        gw.setLocationRelativeTo(null);
        gw.setResizable(false);
        //gw.setResizable(false);
    }
}
