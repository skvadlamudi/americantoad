package svadlamudi.americantoad.move;

import ks.common.games.Solitaire;
import ks.common.model.Column;
import ks.common.model.Stack;
import svadlamudi.americantoad.util.Rank;

/**
 * Move either top card or all cards from given source Tableau to
 * destination tableau.
 */
public class TableauTableauMove extends ks.common.model.Move {
    protected Column from;
    protected Column to;
    protected Column draggingColumn;
    protected int numCards;

    public TableauTableauMove(Column from, Column to, Column draggingColumn, int numCards) {
        this.from = from;
        this.to = to;
        this.draggingColumn = draggingColumn;
        this.numCards = numCards;
    }


    @Override
    public boolean doMove(Solitaire game) {
        // VALIDATE:
        if (valid (game) == false)
            return false;

        // EXECUTE:
        if (draggingColumn == null) {
            from.select(numCards);
            Stack st = from.getSelected();
            to.push(st);
        } else {
            // already have the column
            to.push(draggingColumn);
        }

        return true;
    }

    @Override
    public boolean undo(Solitaire game) {
        // VALIDATE:
        if (to.count() < numCards) return false;

        // We know the number of cards moved, so we select them, extract the
        // stack, and move them all back to the fromPile.
        to.select(numCards);
        Stack st = to.getSelected ();
        from.push(st);

        return true;
    }

    @Override
    public boolean valid(Solitaire game) {
        // VALIDATION:
        boolean validation = false;

        // If move hasn't happened yet, we must extract the desired column to move.
        Column targetColumn;
        if (draggingColumn == null) {
            targetColumn = new Column();
            for (int i = numCards; i >= 1; i--) {
                targetColumn.add (from.peek(from.count() - i));
            }
        } else {
            targetColumn = draggingColumn;
        }

        //   moveColumnBetweenPiles(Column col, BuildablePile to) : to.empty()
        if (to.empty())
            validation = true;

        // 	  moveColumnBetweenPiles(Column col,BuildablePile to) : not col.empty() and col.bottom() == to.rank() - 1 and to.peek().faceUp() == true
        if (!to.empty() && (targetColumn.peek(0).getRank() == Rank.prevRank(to.rank())) && (targetColumn.peek(0).sameSuit(to.peek())))
            validation = true;

        return validation;
    }
}
