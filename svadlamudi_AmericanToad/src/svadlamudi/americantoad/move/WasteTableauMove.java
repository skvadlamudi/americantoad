package svadlamudi.americantoad.move;

import ks.common.games.Solitaire;
import ks.common.model.Card;
import ks.common.model.Column;
import ks.common.model.Pile;
import svadlamudi.americantoad.util.Rank;

/**
 * Move top card of the Waste pile to the given Tableau.
 */
public class WasteTableauMove extends ks.common.model.Move {
    protected Pile waste;
    protected Column tableau;
    protected Card draggingCard;

    public WasteTableauMove(Pile waste, Column tableau, Card draggingCard) {
        this.waste = waste;
        this.tableau = tableau;
        this.draggingCard = draggingCard;
    }

    @Override
    public boolean doMove (Solitaire theGame) {
        // VALIDATE:
        if(valid(theGame) == false)
            return false;

        // EXECUTE:
        // move card from waste to to Pile.
        if(draggingCard == null)
            tableau.add(waste.get());
        else
            tableau.add(draggingCard);

        return true;
    }

    @Override
    public boolean undo(ks.common.games.Solitaire game) {
        // VALIDATE:
        if(tableau.empty()) return false;

        // EXECUTE:
        // remove card and move to waste.
        waste.add(tableau.get());

        return true;
    }

    @Override
    public boolean valid (Solitaire theGame) {

        // VALIDATION:
        boolean validation = false;

        // moveWasteToPile (Waste from, BuildablePile to) : to.empty()
        if (tableau.empty())
            validation = true;

        if (draggingCard == null) {
            if(!waste.peek().isFaceUp())
                return false;
            // 	  moveColumnBetweenPiles(Waste from,BuildablePile to) : not to.empty() and waste.rank() == to.rank() - 1 and to.peek().faceUp()
            else if (!tableau.empty() && (waste.rank() == Rank.prevRank(tableau.rank())) && (waste.peek().sameSuit(tableau.peek())))
                validation = true;

        } else {
            if(!draggingCard.isFaceUp())
                return false;
            // 	  moveColumnBetweenPiles(Waste from,BuildablePile to) : not to.empty() and cardBeingDragged.rank() == to.rank() - 1 and to.peek().faceUp()
            else if (!tableau.empty() && (draggingCard.getRank() == Rank.prevRank(tableau.rank())) && (draggingCard.sameSuit(tableau.peek())))
                validation = true;
        }
        return validation;
    }
}
