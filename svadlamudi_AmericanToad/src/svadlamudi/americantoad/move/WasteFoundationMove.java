package svadlamudi.americantoad.move;

import ks.common.games.Solitaire;
import ks.common.model.Card;
import ks.common.model.Pile;
import svadlamudi.AmericanToad;
import svadlamudi.americantoad.util.Rank;

/**
 * Move top card from Waste pile to given Foundation.
 */
public class WasteFoundationMove extends ks.common.model.Move{

    protected Pile waste;
    protected Pile foundation;
    protected Card draggingCard;

    public WasteFoundationMove(Pile waste, Pile foundation, Card draggingCard) {
        this.waste = waste;
        this.foundation = foundation;
        this.draggingCard = draggingCard;
    }


    @Override
    public boolean doMove(Solitaire game) {
        if(!valid(game))
            return false;

        if(draggingCard == null)
            foundation.add(waste.get());
        else
            foundation.add(draggingCard);

        game.updateScore(+1);
        game.updateNumberCardsLeft(-1);

        return true;
    }

    @Override
    public boolean undo(Solitaire game) {
        if(foundation.empty())
           return false;

        waste.add(foundation.get());
        game.updateScore(-1);
        game.updateNumberCardsLeft(+1);
        return true;
    }

    @Override
    public boolean valid(Solitaire game) {
        if(draggingCard == null) {
            if(!waste.peek().isFaceUp())
                return false;
            else if(foundation.empty() && waste.peek().getRank() == ((AmericanToad) game).getFoundationRank())
                return true;
            else if(waste.peek().sameSuit(foundation.peek()) && waste.peek().getRank() == Rank.nextRank(foundation.peek().getRank()) && foundation.count() < 13)
                return true;
        } else {
            if(!draggingCard.isFaceUp())
                return false;
            else if(foundation.empty() && draggingCard.getRank() == ((AmericanToad) game).getFoundationRank())
                return true;
            else if(draggingCard.sameSuit(foundation.peek()) && draggingCard.getRank() == Rank.nextRank(foundation.peek().getRank()) && foundation.count() < 13)
                return true;
        }

        return false;
    }
}
