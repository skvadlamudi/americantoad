package svadlamudi.americantoad.move;

import svadlamudi.AmericanToad;
import svadlamudi.americantoad.util.Rank;
import ks.common.games.Solitaire;
import ks.common.model.Card;
import ks.common.model.Move;
import ks.common.model.Pile;

/**
 * Move top card from one foundation to another.
 */
public class FoundationFoundationMove extends Move {
	protected Pile src;
	protected Pile dest;
	protected Card draggingCard;
	
	public FoundationFoundationMove(Pile src, Pile dest, Card draggingCard) {
		this.src = src;
		this.dest = dest;
		this.draggingCard = draggingCard;
	}
	
	@Override
	public boolean doMove(Solitaire game) {
		if(!valid(game))
			return false;
		
		if(draggingCard == null)
			dest.add(src.get());
		else
			dest.add(draggingCard);
		
		return true;
	}

	@Override
	public boolean undo(Solitaire game) {
		if(dest.empty())
			return false;
		
		src.add(dest.get());
		
		return true;
	}

	@Override
	public boolean valid(Solitaire game) {
		if(draggingCard == null) {
			if(dest.empty() && src.peek().getRank() == ((AmericanToad)game).getFoundationRank())
				return true;
			else if(!dest.empty() && Rank.nextRank(dest.peek().getRank()) == src.peek().getRank() && dest.peek().getSuit() == src.peek().getSuit() && dest.count() < 13)
				return true;
			else
				return false;
		} else {
			if(dest.empty() && draggingCard.getRank() == ((AmericanToad)game).getFoundationRank())
				return true;
			else if(!dest.empty() && Rank.nextRank(dest.peek().getRank()) == draggingCard.getRank() && dest.peek().getSuit() == draggingCard.getSuit() && dest.count() < 13)
				return true;
			else
				return false;
		}
	}

}
