package svadlamudi.americantoad.move;

import ks.common.games.Solitaire;
import ks.common.model.MultiDeck;
import ks.common.model.Pile;

/**
 * Deal one card to the Waste from the Stock.
 */
public class DealStockMove extends ks.common.model.Move {

    protected MultiDeck stock;
    protected Pile waste;

    public DealStockMove(MultiDeck stock, Pile waste) {
        this.stock = stock;
        this.waste = waste;
    }

    @Override
    public boolean doMove(Solitaire game) {
        // VALIDATE
        if (!valid(game))
            return false;

        // EXECUTE:
        // Get card from deck
        waste.add(stock.get());

        game.updateNumberCardsLeft(-1);
        return true;
    }

    @Override
     public boolean undo(Solitaire game) {
        // VALIDATE:
        if (waste.empty())
            return false;

        // UNDO:
        stock.add (waste.get());

        // update the number of cards to go.
        game.updateNumberCardsLeft(+1);
        return true;
    }

    @Override
     public boolean valid(Solitaire game) {
        // VALIDATION:
        boolean validation = false;

        // dealCard(stock,waste) : not deck.empty()
        if (!stock.empty())
            validation = true;

        return validation;
    }
}
