package svadlamudi.americantoad.move;

import ks.common.games.Solitaire;
import ks.common.model.MultiDeck;
import ks.common.model.Pile;
import svadlamudi.AmericanToad;

/**
 * Move cards from the waste pile to the stock pile.
 */
public class ResetStockMove extends ks.common.model.Move{

    protected MultiDeck stock;
    protected Pile waste;

    public ResetStockMove(MultiDeck stock, Pile waste) {
        this.stock = stock;
        this.waste = waste;
    }


    @Override
    public boolean doMove(Solitaire game) {
        if(!valid(game))
            return false;

        while(!waste.empty()){
            stock.add(waste.get());
            game.updateNumberCardsLeft(+1);
        }

        ((AmericanToad)game).updateNumPasses(+1);

        return true;
    }

    @Override
    public boolean undo(Solitaire game) {
        if(stock.empty() || !waste.empty() || ((AmericanToad)game).getNumPasses() == 0)
            return false;

        while(!stock.empty()){
            waste.add(stock.get());
            game.updateNumberCardsLeft(-1);
        }

        ((AmericanToad)game).updateNumPasses(-1);

        return true;
    }

    @Override
    public boolean valid(Solitaire game) {
        if(stock.empty()==false || waste.empty() || ((AmericanToad)game).getNumPasses() == ((AmericanToad)game).numPassesAllowed-1)
            return false;

        return true;
    }
}
