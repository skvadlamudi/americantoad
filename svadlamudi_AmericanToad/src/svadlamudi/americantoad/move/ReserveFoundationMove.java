package svadlamudi.americantoad.move;

import ks.common.games.Solitaire;
import ks.common.model.Card;
import ks.common.model.Pile;
import svadlamudi.AmericanToad;
import svadlamudi.americantoad.util.Rank;

/**
 * Handle a Reserve to Foundation move.
 */
public class ReserveFoundationMove extends ks.common.model.Move{

    protected Pile reserve;
    protected Pile foundation;
    protected Card draggingCard;

    public ReserveFoundationMove(Pile reserve, Pile foundation, Card draggingCard) {

        super();

        this.reserve = reserve;
        this.foundation = foundation;
        this.draggingCard = draggingCard;
    }

    @Override
    public boolean doMove(Solitaire game) {
        if(valid(game) == false)
            return false;

        if(draggingCard == null)
            foundation.add(reserve.get());
        else
            foundation.add(draggingCard);

        if(!reserve.empty()) {
            Card topCard = reserve.get();
            topCard.setFaceUp(true);
            reserve.add(topCard);
        }

        game.updateScore(+1);

        return true;
    }

    @Override
    public boolean undo(Solitaire game) {
        if(foundation.empty())
            return false;

        reserve.add(foundation.get());
        game.updateScore(-1);

        return true;
    }

    @Override
    public boolean valid(Solitaire game) {

        if(draggingCard == null) {
            Card c = reserve.peek();

            if(!c.isFaceUp())
                return false;
            else if(foundation.empty() && c.getRank() == ((AmericanToad)game).getFoundationRank())
                return true;
            else if(c.sameSuit(foundation.peek()) && c.getRank() == Rank.nextRank(foundation.peek().getRank()) && foundation.count() < 13)
                return true;
        } else {
            if(!draggingCard.isFaceUp())
                return false;
            else if(foundation.empty() && draggingCard.getRank() == ((AmericanToad)game).getFoundationRank())
                return true;
            else if(draggingCard.sameSuit(foundation.peek()) && draggingCard.getRank() == Rank.nextRank(foundation.peek().getRank()) && foundation.count() < 13)
                return true;

        }

        return false;
    }
}
