package svadlamudi.americantoad.move;

import ks.common.games.Solitaire;
import ks.common.model.Card;
import ks.common.model.Column;
import ks.common.model.Pile;
import svadlamudi.AmericanToad;
import svadlamudi.americantoad.util.Rank;

/**
 * Move the top card from given Tableau to top of Foundation.
 */
public class TableauFoundationMove extends ks.common.model.Move{

    protected Column tableau;
    protected Pile foundation;
    protected Column draggingColumn;

    public TableauFoundationMove(Column tableau, Pile foundation, Column draggingColumn) {
        this.tableau = tableau;
        this.foundation = foundation;
        this.draggingColumn = draggingColumn;
    }


    @Override
    public boolean doMove(Solitaire game) {
        if(!valid(game))
            return false;

        if(draggingColumn == null)
            foundation.add(tableau.get());
        else
            foundation.add(draggingColumn.get());

        game.updateScore(+1);

        return true;
    }

    @Override
    public boolean undo(Solitaire game) {
        if(foundation.empty())
            return false;

        tableau.add(foundation.get());
        game.updateScore(-1);
        return true;
    }

    @Override
    public boolean valid(Solitaire game) {
        if(draggingColumn == null) {
            Card c = tableau.peek();

            if(c.sameSuit(foundation.peek()) && c.getRank() == Rank.nextRank(foundation.peek().getRank()) && foundation.count() < 13)
                return true;
            else if(foundation.empty() && c.getRank() == ((AmericanToad)game).getFoundationRank())
                return true;
        } else {
            if(draggingColumn.peek().sameSuit(foundation.peek()) && draggingColumn.peek().getRank() == Rank.nextRank(foundation.peek().getRank()) && foundation.count() < 13)
                return true;
            else if(foundation.empty() && draggingColumn.peek().getRank() == ((AmericanToad)game).getFoundationRank())
                return true;
        }
        return false;
    }
}
