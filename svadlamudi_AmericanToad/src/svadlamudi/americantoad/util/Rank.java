package svadlamudi.americantoad.util;

/**
 * Help with certain functions with Card ranks.
 */
public class Rank {

    private static final String[] ranks = {"", "Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};

    public static int nextRank(int rank) {
        if(rank == 13)
            return 1;
        else
            return rank+1;
    }

    public static int prevRank(int rank) {
        if(rank == 1)
            return 13;
        else
            return rank-1;
    }

    public static String rankToText(int rank) {
        return ranks[rank];
    }

}
