package svadlamudi.americantoad.controller;

import ks.common.controller.SolitaireReleasedAdapter;
import ks.common.model.Move;
import ks.common.model.MultiDeck;
import ks.common.model.Pile;
import svadlamudi.AmericanToad;
import svadlamudi.americantoad.move.DealStockMove;
import svadlamudi.americantoad.move.ResetStockMove;

import java.awt.event.MouseEvent;

/**
 * Handle MouseEvents on the Stock pile.
 */
public class StockController extends SolitaireReleasedAdapter {

    protected AmericanToad theGame;
    protected MultiDeck stock;
    protected Pile waste;

    public StockController(AmericanToad theGame, MultiDeck stock, Pile waste) {

        super (theGame);

        this.theGame = theGame;
        this.stock = stock;
        this.waste = waste;
    }

    public void mouseClicked(MouseEvent me) {
        if(stock.empty()) {
            Move m = new ResetStockMove(stock, waste);
            if (m.doMove(theGame)) {

                // Success! Add this move to our history.
                theGame.pushMove (m);
                theGame.refreshWidgets();
            }
        } else {
            Move m = new DealStockMove(stock, waste);
            if (m.doMove(theGame)) {
                theGame.pushMove (m);     // Successful Deal Move

                theGame.refreshWidgets(); // refresh updated widgets.
            }
        }
    }

    /**
     * Coordinate reaction to the beginning of a Drag Event. In this case,
     * no drag is ever achieved, and we simply deal upon the press
     */
    public void mousePressed (java.awt.event.MouseEvent me) {}
}
