package svadlamudi.americantoad.controller;

import ks.common.controller.SolitaireReleasedAdapter;
import ks.common.model.*;
import ks.common.view.CardView;
import ks.common.view.ColumnView;
import ks.common.view.Container;
import ks.common.view.Widget;
import svadlamudi.AmericanToad;
import svadlamudi.americantoad.move.*;

import java.awt.event.MouseEvent;

/**
 * Handle MouseEvents on a given Tableau
 */
public class TableauController extends SolitaireReleasedAdapter {

    protected AmericanToad theGame;
    protected ColumnView srcView;

    public TableauController(AmericanToad theGame, ColumnView srcView) {

        super(theGame);

        this.theGame = theGame;
        this.srcView = srcView;
    }

    /**
     * Try to play the faceup card directly to the to.
     *
     * @param me java.awt.event.MouseEvent
     */
    public void mouseClicked(MouseEvent me) {

        if(me.getClickCount() > 1) {

            // Point to our underlying model element.
            Column src = (Column) srcView.getModelElement();

            if(src.empty())
                return;

            // See if we can move this one card.
            boolean moveMade = false;
            for (int f = 1; f <= 8; f++) {
                Pile fp = (Pile) theGame.getModelElement ("foundation" + f);
                Move m = new TableauFoundationMove(src, fp, null);
                if (m.doMove(theGame)) {

                    // Success! Add this move to our history.
                    theGame.pushMove (m);
                    if(src.empty()) {
	                    ReserveTableauMove rtm = new ReserveTableauMove(theGame.getReserve(), src, null);
	                    if(rtm.doMove(theGame))
	                    	theGame.pushMove(rtm);
                    }
                    moveMade = true;
                    theGame.refreshWidgets();
                    break;
                }
            }

            if (!moveMade) {
                java.awt.Toolkit.getDefaultToolkit().beep();
                return; // announce our displeasure
            }
        }
    }

    /**
     * Coordinate reaction to the beginning of a Drag Event.
     *
     * Note: There is no way to differentiate between a press that
     *       will become part of a double click vs. a click that will
     *       be held and dragged. Only mouseReleased will be able to
     *       help us out with that one.
     *
     * Creation date: (10/4/01 6:05:55 PM)
     * @param me java.awt.event.MouseEvent
     */
    public void mousePressed(MouseEvent me) {

        // The container manages several critical pieces of information; namely, it
        // is responsible for the draggingObject; in our case, this would be a CardView
        // Widget managing the card we are trying to drag between two piles.
        Container c = theGame.getContainer();

        //** Return if there is no card to be chosen. *//*
        Column src = (Column) srcView.getModelElement();
        if (src.count() == 0) {
            return;
        }

        // Get a column of cards to move from the BuildablePileView
        // Note that this method will alter the model for BuildablePileView if the condition is met.
        ColumnView colView = srcView.getColumnView(me);

        // an invalid selection (either all facedown, or not in faceup region)
        if (colView == null) {
            return;
        }

        // Check conditions
        Column col = (Column) colView.getModelElement();
        if (col == null) {
            System.err.println ("TableauController::mousePressed(): Unexpectedly encountered a ColumnView with no Column.");
            return; // sanity check, but should never happen.
        }

        // verify that Column has desired AmericanToad Properties to move
        if ((!col.descending()) || (!col.sameSuit()) || !((col.count() == 1) || (src.empty()))) {
            src.push(col);
            java.awt.Toolkit.getDefaultToolkit().beep();
            return; // announce our displeasure
        }

        // If we get here, then the user has indeed clicked on the top card in the PileView and
        // we are able to now move it on the screen at will. For smooth action, the bounds for the
        // cardView widget reflect the original card location on the screen.
        Widget w = c.getActiveDraggingObject();
        if (w != Container.getNothingBeingDragged()) {
            System.err.println ("TableauController::mousePressed(): Unexpectedly encountered a Dragging Object during a Mouse press.");
            return;
        }

        // Tell container which object is being dragged, and where in that widget the user clicked.
        c.setActiveDraggingObject (colView, me);

        // Tell container which BuildablePileView is the source for this drag event.
        c.setDragSource (srcView);

        // we simply redraw our source pile to avoid flicker,
        // rather than refreshing all widgets...
        srcView.redraw();
    }

    /**
     * Coordinate reaction to the completion of a Drag Event.
     * <p>
     * A bit of a challenge to construct the appropriate move, because cards
     * can be dragged both from the WastePile (as a CardView widget) and the
     * BuildablePileView (as a ColumnView widget).
     * <p>
     * @param me java.awt.event.MouseEvent
     */
    public void mouseReleased(MouseEvent me) {
        Container c = theGame.getContainer();

        /** Return if there is no card being dragged chosen. */
        Widget w = c.getActiveDraggingObject();
        if (w == Container.getNothingBeingDragged()) {
            c.releaseDraggingObject();
            return;
        }

        /** Recover the from BuildablePile OR waste Pile */
        Widget fromWidget = c.getDragSource();
        if (fromWidget == null) {
            System.err.println ("TableauController::mouseReleased(): somehow no dragSource in container.");
            c.releaseDraggingObject();
            return;
        }

        // Determine the To Pile
        Column src = (Column) srcView.getModelElement();

        if (fromWidget instanceof ColumnView) {
            // Must be a ColumnView widget being dragged.
            ColumnView columnView = (ColumnView) w;
            Column col = (Column) columnView.getModelElement();
            if (col == null) {
                System.err.println ("TableauController::mouseReleased(): somehow ColumnView model element is null.");
                return;
            }

            if (fromWidget == srcView) {
                src.push(col);   // simply put right back where it came from. No move
            } else {
                Column fromPile = (Column) fromWidget.getModelElement();
                Move m = new TableauTableauMove(fromPile, src, col, col.count());

                if (m.doMove (theGame)) {
                    // Successful move! add move to our set of moves
                    theGame.pushMove (m);
                    if(fromPile.empty()) {
	                    ReserveTableauMove rtm = new ReserveTableauMove(theGame.getReserve(), fromPile, null);
	                    if(rtm.doMove(theGame))
	                    	theGame.pushMove(rtm);
                    }
                } else {
                    // Invalid move. Restore to original column. NO MOVE MADE
                    fromPile.push (col);
                }
            }
        } else if(fromWidget.getModelElement().getName().contains("reserve")) {

            CardView cardView = (CardView) w;
            Card theCard = (Card) cardView.getModelElement();
            if (theCard == null) {
                System.err.println ("TableauController::mouseReleased(): somehow CardView model element is null.");
                return;
            }

            Pile fromPile = (Pile) fromWidget.getModelElement();
            Move m = new ReserveTableauMove(fromPile, src, theCard);
            if (m.doMove (theGame)) {
                // Successful move! add move to our set of moves
                theGame.pushMove (m);
            } else {
                // Invalid move. Restore to original column. NO MOVE MADE
                fromPile.add(theCard);
            }
        } else if(fromWidget.getModelElement().getName().contains("waste")) {
            CardView cardView = (CardView) w;
            Card theCard = (Card) cardView.getModelElement();
            if (theCard == null) {
                System.err.println ("TableauController::mouseReleased(): somehow CardView model element is null.");
                return;
            }

            Pile fromPile = (Pile) fromWidget.getModelElement();
            Move m = new WasteTableauMove(fromPile, src, theCard);
            if (m.doMove (theGame)) {
                // Successful move! add move to our set of moves
                theGame.pushMove (m);
            } else {
                // Invalid move. Restore to original column. NO MOVE MADE
                fromPile.add(theCard);
            }
        }
        // release the dragging object, (container will reset dragSource)
        c.releaseDraggingObject();
        c.repaint();
    }
}
