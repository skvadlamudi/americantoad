package svadlamudi.americantoad.controller;

import ks.common.controller.SolitaireReleasedAdapter;
import ks.common.model.*;
import ks.common.view.*;
import svadlamudi.AmericanToad;
import svadlamudi.americantoad.move.FoundationFoundationMove;
import svadlamudi.americantoad.move.ReserveFoundationMove;
import svadlamudi.americantoad.move.ReserveTableauMove;
import svadlamudi.americantoad.move.TableauFoundationMove;
import svadlamudi.americantoad.move.WasteFoundationMove;

import java.awt.event.MouseEvent;

/**
 * Handle the MouseEvents on a given Foundation.
 */
public class FoundationController extends SolitaireReleasedAdapter {

    protected AmericanToad theGame;
    protected PileView src;

    public FoundationController(AmericanToad theGame, PileView src) {
    	super(theGame);
        this.theGame = theGame;
        this.src = src;
    }
    
    public void mousePressed(MouseEvent me) {
    	// The container manages several critical pieces of information; namely, it
        // is responsible for the draggingObject; in our case, this would be a CardView
        // Widget managing the card we are trying to drag between two piles.
        Container c = theGame.getContainer();

        /** Return if there is no card to be chosen. */
        Pile foundation = (Pile) src.getModelElement();
        if (foundation.count() == 0) {
            c.releaseDraggingObject();
            return;
        }

        // Get a card to move from PileView. Note: this returns a CardView.
        // Note that this method will alter the model for BuildablePileView if the condition is met.
        CardView cardView = src.getCardViewForTopCard(me);

        // an invalid selection of some sort.
        if (cardView == null) {
            c.releaseDraggingObject();
            return;
        }

        // If we get here, then the user has indeed clicked on the top card in the PileView and
        // we are able to now move it on the screen at will. For smooth action, the bounds for the
        // cardView widget reflect the original card location on the screen.
        Widget w = c.getActiveDraggingObject();
        if (w != Container.getNothingBeingDragged()) {
            System.err.println ("ReserveController::mousePressed(): Unexpectedly encountered a Dragging Object during a Mouse press.");
            return;
        }

        // Tell container which object is being dragged, and where in that widget the user clicked.
        c.setActiveDraggingObject (cardView, me);

        // Tell container which source widget initiated the drag
        c.setDragSource (src);

        // The only widget that could have changed is ourselves. If we called refresh, there
        // would be a flicker, because the dragged widget would not be redrawn. We simply
        // force the WastePile's image to be updated, but nothing is refreshed on the screen.
        // This is patently OK because the card has not yet been dragged away to reveal the
        // card beneath it.  A bit tricky and I like it!
        src.redraw();
    }

    /**
     * Coordinate reaction to the completion of a Drag Event.
     * <p>
     * A bit of a challenge to construct the appropriate move, because cards
     * can be dragged both from the WastePile (as a CardView widget) and the
     * BuildablePileView (as a ColumnView widget).
     * <p>
     * @param me java.awt.event.MouseEvent
     */
    public void mouseReleased(MouseEvent me) {
        Container c = theGame.getContainer();

        /** Return if there is no card being dragged chosen. */
        Widget w = c.getActiveDraggingObject();
        if (w == Container.getNothingBeingDragged()) {
            c.releaseDraggingObject();
            return;
        }

        /** Recover the from BuildablePile OR waste Pile */
        Widget fromWidget = c.getDragSource();
        if (fromWidget == null) {
            System.err.println ("FoundationController::mouseReleased(): somehow no dragSource in container.");
            c.releaseDraggingObject();
            return;
        }

        // Determine the To Pile
        Pile toPile = (Pile) src.getModelElement();

        if (fromWidget instanceof ColumnView) {
            // Must be a ColumnView widget being dragged.
            ColumnView columnView = (ColumnView) w;
            Column col = (Column) columnView.getModelElement();
            if (col == null) {
                System.err.println ("FoundationController::mouseReleased(): somehow ColumnView model element is null.");
                return;
            }

            if (fromWidget == src) {
                toPile.push(col);   // simply put right back where it came from. No move
            } else {
                Column fromPile = (Column) fromWidget.getModelElement();
                Move m = new TableauFoundationMove(fromPile, toPile, col);

                if (m.doMove (theGame)) {
                    // Successful move! add move to our set of moves
                    theGame.pushMove (m);
                    if(fromPile.empty()) {
	                    ReserveTableauMove rtm = new ReserveTableauMove(theGame.getReserve(), fromPile, null);
	                    if(rtm.doMove(theGame))
	                    	theGame.pushMove(rtm);
                    }
                } else {
                    // Invalid move. Restore to original column. NO MOVE MADE
                    fromPile.push(col);
                }
            }
        } else if(fromWidget.getModelElement().getName().contains("reserve")){
            // From the reserve
            CardView cardView = (CardView) w;
            Card theCard = (Card) cardView.getModelElement();
            if (theCard == null) {
                System.err.println ("FoundationController::mouseReleased(): somehow CardView model element is null.");
                return;
            }

            Pile reserve = (Pile) fromWidget.getModelElement();
            Move m = new ReserveFoundationMove (reserve, toPile, theCard);
            if (m.doMove (theGame)) {
                // Successful move! add move to our set of moves
                theGame.pushMove (m);

            } else {
                // Invalid move. Restore to original waste pile. NO MOVE MADE
                reserve.add (theCard);
            }
        } else if(fromWidget.getModelElement().getName().contains("waste")) {
            //Must be from the WastePile
            CardView cardView = (CardView) w;
            Card theCard = (Card) cardView.getModelElement();
            if (theCard == null) {
                System.err.println ("FoundationController::mouseReleased(): somehow CardView model element is null.");
                return;
            }

            Pile wastePile = (Pile) fromWidget.getModelElement();
            Move m = new WasteFoundationMove(wastePile, toPile, theCard);
            if (m.doMove (theGame)) {
                // Successful move! add move to our set of moves
                theGame.pushMove (m);
            } else {
                // Invalid move. Restore to original waste pile. NO MOVE MADE
                wastePile.add (theCard);
            }
        } else {
        	//Must be from the FoundationPile
        	CardView cardView = (CardView) w;
            Card theCard = (Card) cardView.getModelElement();
            if (theCard == null) {
                System.err.println ("FoundationController::mouseReleased(): somehow CardView model element is null.");
                return;
            }

            Pile srcPile = (Pile) fromWidget.getModelElement();
            Move m = new FoundationFoundationMove(srcPile, toPile, theCard);
            if (m.doMove (theGame)) {
                // Successful move! add move to our set of moves
                theGame.pushMove (m);
                
            } else {
                // Invalid move. Restore to original waste pile. NO MOVE MADE
                srcPile.add (theCard);
            }
        }
        // release the dragging object, (container will reset dragSource)
        c.releaseDraggingObject();
        c.repaint();
    }
    
}
